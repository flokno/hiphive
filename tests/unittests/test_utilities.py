import unittest
import numpy as np

from ase.build import bulk
from hiphive.utilities import get_displacements, get_neighbor_shells


class TestUtilities(unittest.TestCase):
    """
    Unittest class for utility functions.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def test_get_displacements(self):
        """
        Test get_displacements function
        """
        for symbol in ['Al', 'Ti', 'Si']:
            atoms_ideal = bulk(symbol).repeat(4)

            atoms = atoms_ideal.copy()
            atoms.rattle(1.0)  # This causes atoms to go outside cell
            ref_disps = atoms.positions - atoms_ideal.positions

            atoms.wrap()
            disps = get_displacements(atoms, atoms_ideal)
            np.testing.assert_almost_equal(ref_disps, disps)

    def test_get_shells(self):

        # FCC
        atoms = bulk('Al', a=1.0)
        expected_fcc_dists = [1/np.sqrt(2), 1.0, np.sqrt(1.5), np.sqrt(2)]
        shells = get_neighbor_shells(atoms, cutoff=1.5)
        self.assertEqual(len(shells), len(expected_fcc_dists))
        for shell, dist in zip(shells, expected_fcc_dists):
            self.assertEqual(shell.types, ('Al', 'Al'))
            self.assertAlmostEqual(shell.distance, dist)

        # HCP
        atoms = bulk('Al', 'hcp', a=1.0)
        expected_hcp_dists = [1, np.sqrt(2), np.sqrt(8/3)]
        shells = get_neighbor_shells(atoms, cutoff=1.65)
        self.assertEqual(len(shells), len(expected_hcp_dists))
        for shell, dist in zip(shells, expected_hcp_dists):
            self.assertEqual(shell.types, ('Al', 'Al'))
            self.assertAlmostEqual(shell.distance, dist)

        # rocksalt
        atoms = bulk('NaCl', 'rocksalt', a=1.0)
        expected_nacl_shells = [(('Cl', 'Na'), 0.5),
                                (('Cl', 'Cl'), 1/np.sqrt(2)),
                                (('Na', 'Na'), 1/np.sqrt(2)),
                                (('Cl', 'Na'), np.sqrt(3)/2),
                                (('Cl', 'Cl'), 1),
                                (('Na', 'Na'), 1)]
        shells = get_neighbor_shells(atoms, cutoff=1.05)
        self.assertEqual(len(shells), len(expected_nacl_shells))
        for shell, (types, dist) in zip(shells, expected_nacl_shells):
            self.assertEqual(shell.types, types)
            self.assertAlmostEqual(shell.distance, dist)

        # test case where two shells will be within 2*dist_tol
        dist_tol = 1e-5
        atoms = bulk('Al', a=1.0).repeat(3)
        atoms[0].x += 1.5 * dist_tol
        shells = get_neighbor_shells(atoms, cutoff=1.05, dist_tol=dist_tol)


if __name__ == '__main__':
    unittest.main()
