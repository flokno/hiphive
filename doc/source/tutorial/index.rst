.. _tutorial:
.. index::
   single: Tutorial

Tutorial
********

This tutorial serves as a hands-on introduction to :program:`hiPhive`
and provides an overview of its key features. Various topics topics
are described in the `advanced topics section <advanced_topics>`_.

The scripts and database that are required for this tutorial can
be `downloaded as a single zip archive
<https://hiphive.materialsmodeling.org/tutorial.zip>`_. These
scripts will be compatible with the latest stable release. If you want
to download the scripst from the development version `download this
archive
<https://hiphive.materialsmodeling.org/dev/tutorial.zip>`_
instead.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   prepare_reference_data
   construct_fcp
   compute_harmonic_thermal_properties
   compute_third_order_properties
   run_fourth_order_MD
