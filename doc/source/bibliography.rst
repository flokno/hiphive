.. _bibliography:
.. index:: Bibliography

Bibliography
************

.. [AndSanAsp12]
   | X. Andrade, J. N. Sanders, and A. Aspuru-Guzik
   | *Application of compressed sensing to the simulation of atomic systems*
   | Proceedings of the National Academy of Sciences **109**, 13928 (2012)
   | `doi: 10.1073/pnas.1209890109 <http://dx.doi.org/10.1073/pnas.1209890109>`_

.. [BorHua98]
   | M. Born and K. Huang
   | *Dynamical Theory of Crystal Lattices*
   | Oxford University Press (1998)
   | `ISBN: 0198503695 <https://global.oup.com/academic/product/dynamical-theory-of-crystal-lattices-9780198503699>`_

.. [CanWak08]
   | E. J. Candès and M. B. Wakin
   | *An Introduction To Compressive Sampling*
   | Signal Processing Magazine, IEEE **25**, 201 (2008)
   | `doi: 10.1109/MSP.2007.914731 <http://dx.doi.org/10.1109/MSP.2007.914731>`_

.. [ErrCalMau14]
   | I. Errea, M. Callandra, and F. Mauri
   | *Anharmonic free energies and phonon dispersions from the stochastic self-consistent harmonic approximation: Application to platinum and palladium hydrides*
   | Physical Review B **89**, 064302 (2014)
   | `doi: 10.1103/PhysRevB.89.064302 <http://dx.doi.org/10.1103/PhysRevB.89.064302>`_

.. [Ful10]
   | B. Fultz
   | *Vibrational thermodynamics of materials*
   | Progress in Materials Science **55**, 247 (2010)
   | `doi: 10.1016/j.pmatsci.2009.05.002 <http://dx.doi.org/10.1016/j.pmatsci.2009.05.002>`_

.. [GolOsh09]
   | T. Goldstein and S. Osher
   | *The Split Bregman Method for L1-Regularized Problems*
   | SIAM Journal of Imaging Science **2**, 323 (2009)
   | `doi: 10.1137/080725891 <http://dx.doi.org/10.1137/080725891>`_

.. [LiCarKat14]
   | W. Li, J. Carrete, N. A. Katcho, and N. Mingo
   | *ShengBTE: A solver of the Boltzmann transport equation for phonons*
   | Computer Physics Communications **185**, 1747 (2014)
   | `doi: 10.1016/j.cpc.2014.02.015 <http://dx.doi.org/10.1016/j.cpc.2014.02.015>`_

.. [PedVarGra11]
   | F. Pedregosa *et al.*,
   | *Scikit-Learn: Machine Learning in Python*,
   | Journal of Machine Learning Research **12**, 2825 (2011)

.. [SanAndAsp15]
   | J. N. Sanders, X. Andrade, and Alán Aspuru-Guzik
   | *Compressed Sensing for the Fast Computation of Matrices: Application to Molecular Vibrations*,
   | ACS Central Science **1**, 24 (2015);
   | `doi: 10.1021/oc5000404 <http://dx.doi.org/10.1021/oc5000404>`_

.. [TogTan15]
   | A. Togo and I Tanaka
   | *First principles phonon calculations in materials science*,
   | Scripta Materialia **108**, 1 (2015);
   | `doi: 10.1016/j.scriptamat.2015.07.021 <http://dx.doi.org/10.1016/j.scriptamat.2015.07.021>`_

.. [TogChaTan15]
   | A. Togo, L. Chaput, and I. Tanaka
   | *Distributions of phonon lifetimes in Brillouin zones*,
   | Physical Review B **91**, 094306 (2015);
   | `doi: 10.1103/PhysRevB.91.094306 <http://dx.doi.org/10.1103/PhysRevB.91.094306>`_

.. [TadGohTsu14]
   | T. Tadano, Y. Gohda, and S. Tsuneyuki
   | *Anharmonic force constants extracted from first-principles molecular dynamics: applications to heat transfer simulations*,
   | Physical Review B **91**, 094306 (2015);
   | `doi: 10.1103/PhysRevB.91.094306 <http://dx.doi.org/10.1103/PhysRevB.91.094306>`_

.. [Wal98]
   | D. C. Wallace
   | *Thermodynamics of Crystals*
   | Dover Publications (1998)
   | ISBN: 0-486-40212-6

.. [ZhoNieXia14]
   | F. Zhou, W. Nielson, Y. Xia, and V. Ozolins
   | *Lattice anharmonicity and thermal conductivity from compressive sensing of first-principles calculations*,
   | Physical Review Letters **113**, 185501 (2014);
   | `doi: 10.1103/PhysRevLett.113.185501 <http://dx.doi.org/10.1103/PhysRevLett.113.185501>`_

.. [Zim60]
   | J. M. Ziman
   | *Electrons and Phonons*
   | Oxford University Press (1960)
