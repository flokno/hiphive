"""
This script carries a series of molecular dynamics simulations at different
temperatures and stores snapshots to file.

This scripts runs in approximately 6 minutes on an Intel Core i7-6700K CPU.
"""

import os
from ase.io import write, read
from ase.build import bulk
from ase.calculators.emt import EMT
from ase.io.trajectory import Trajectory

from ase import units
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.langevin import Langevin
from ase.md import MDLogger

if not os.path.exists('md_runs'):
    os.makedirs('md_runs')


# parameters
temperatures = [200, 800, 1400]
number_of_equilibration_steps = 500
number_of_production_steps = 500
time_step = 5
dump_interval = 20

# setup system
atoms_ideal = bulk('Ni').repeat(5)
calc = EMT()

for temperature in temperatures:

    print('Temperature: {}'.format(temperature))

    # set up molecular dynamics simulation
    atoms = atoms_ideal.copy()
    atoms.set_calculator(calc)
    dyn = Langevin(atoms, time_step * units.fs, temperature * units.kB, 0.02)

    # equilibration run
    MaxwellBoltzmannDistribution(atoms, temperature * units.kB)
    dyn.run(number_of_equilibration_steps)

    # production run
    log_file = 'md_runs/T{:}.log'.format(temperature)
    traj_file = 'md_runs/T{:}.traj'.format(temperature)
    logger = MDLogger(dyn, atoms, log_file,
                      header=True, stress=False,
                      peratom=True, mode='w')
    traj_writer = Trajectory(traj_file, 'w', atoms)
    dyn.attach(logger, interval=dump_interval)
    dyn.attach(traj_writer.write, interval=dump_interval)
    dyn.run(number_of_production_steps)

    # prepare snapshots for later use
    frames = []
    for atoms in read(traj_file, ':'):
        forces = atoms.get_forces()
        displacements = atoms.positions - atoms_ideal.get_positions()
        atoms.positions = atoms_ideal.get_positions()
        atoms.new_array('displacements', displacements)
        atoms.new_array('forces', forces)
        frames.append(atoms.copy())
    print(' Number of snapshots: {}'.format(len(frames)))
    write('md_runs/snapshots_T{:}.xyz'.format(temperature),
          frames, format='extxyz')
